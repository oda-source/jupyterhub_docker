ARG BASE_IMAGE=jupyterhub/jupyterhub:1.2@sha256:85e046c398e1ae5fd5981e227e966a011859285f0e26626031525f72cde23384
FROM $BASE_IMAGE AS builder

ARG PYTHON37_VER=3.7.5
ARG PYTHON36_VER=3.6.8
ARG OPTIMIZE_PYTHON=false
ARG MAKE_WORKERS=8
ARG INSTALL_MYPATIENT_PYTHON=true
ARG INSTALL_MRCNN=true
# Setup Jupyterhub user
ARG USER=jupyterhub
ARG UID=1000
ARG GID=1000
ARG PW=jupyterhub

ENV CONFIG_PATH=/srv/jupyterhub/jupyterhub_config.py
ENV REQUESTS_CA_BUNDLE=/etc/ssl/certs/ca-certificates.crt

RUN apt-get update -y

RUN apt-get install -y sudo netcat

RUN useradd -m ${USER} --uid=${UID} && echo "${USER}:${PW}" | \
    chpasswd && adduser ${USER} sudo
RUN usermod -a -G shadow ${USER}
RUN echo "${USER}     ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
RUN chgrp sudo /etc/passwd
RUN chmod g+w /etc/passwd
RUN chmod g+w /etc/shadow
RUN chown -R ${USER} /etc/ssl/certs

RUN pip install notebook
RUN pip install oauthenticator
RUN pip install sudospawner
RUN pip install virtualenv
RUN pip install jupyterlab
RUN apt-get install -y build-essential
RUN apt-get install -y libssl-dev zlib1g-dev libncurses5-dev libncursesw5-dev libreadline-dev libsqlite3-dev
RUN apt-get install -y libgdbm-dev libdb5.3-dev libbz2-dev libexpat1-dev liblzma-dev libffi-dev uuid-dev libglib2.0-0
RUN apt-get install -y libsm6 libxext6 libxrender-dev
RUN apt-get install -y openjdk-8-jdk

COPY modules/ /modules/
COPY extra_data/ /extra_data/

# Install python 3.7 from source
WORKDIR /python37/
COPY scripts/install_python_ver.sh /python37/install_python_ver.sh
RUN bash install_python_ver.sh ${PYTHON37_VER} python3.7

# Install python 3.6 from source
WORKDIR /python36/
COPY scripts/install_python_ver.sh /python36/install_python_ver.sh
RUN bash install_python_ver.sh ${PYTHON36_VER} python3.6

RUN ldconfig

RUN pip install psycopg2-binary

#RUN useradd -m -p $(openssl passwd -1 ${ADMIN_PW}) -s /bin/bash -G sudo ${JUPYTER_ADMIN}


WORKDIR /srv/jupyterhub/

COPY jupyterhub_config.py .
COPY scripts/start_jupyterhub.sh /srv/jupyterhub/start_jupyterhub.sh
RUN chmod +x /srv/jupyterhub/start_jupyterhub.sh

RUN chown -R ${USER}:${GID} /srv/jupyterhub
USER ${USER}:${GID}
RUN sudo mkdir /demo_notebooks/
RUN sudo ln -s /demo_notebooks /etc/skel/demo_notebooks

CMD /srv/jupyterhub/start_jupyterhub.sh
