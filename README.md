# ODA's Jupyterhub Instance

This repository is for building ODA's customized Jupyterhub. Built using Jupyterhub's base Dockerfile found at: https://github.com/jupyterhub/jupyterhub/blob/master/Dockerfile. It has been modified to enable more enterprise deployment. 

## Building

A docker image can be built using either `docker build -t oda_jupyterhub:latest .` or `docker-compose build`

