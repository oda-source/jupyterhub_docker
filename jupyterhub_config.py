# jupyterhub_config.py
c = get_config()

import logging
import os

logger = logging.getLogger(__name__)

pjoin = os.path.join

runtime_dir = os.path.join('/srv/jupyterhub')
ssl_dir = pjoin(runtime_dir, 'ssl')
if not os.path.exists(ssl_dir):
    os.makedirs(ssl_dir)


c.JupyterHub.port = int(os.getenv('PORT', default='8000'))
c.JupyterHub.hub_port = int(os.getenv('HUB_PORT', default = '8081'))

# put the JupyterHub cookie secret and state db
# in /var/run/jupyterhub
c.JupyterHub.cookie_secret_file = pjoin(runtime_dir, 'cookie_secret')
c.JupyterHub.db_url = os.getenv('DB_URL',default = pjoin(runtime_dir, 'jupyterhub.sqlite'))
# or `--db=/path/to/jupyterhub.sqlite` on the command-line

# SSL Setup.
use_ssl = os.getenv('ENABLE_SSL', default = False)
if use_ssl:
    print('Using SSL')
    c.JupyterHub.ssl_key = os.getenv('SSL_KEY', '/var/ssl/my.key')
    c.JupyterHub.ssl_cert = os.getenv('SSL_CERT', '/var/ssl/my.cert')

# put the log file in /var/log
c.JupyterHub.extra_log_file = '/srv/jupyterhub/jupyterhub.log'
c.JupyterHub.spawner_class='sudospawner.SudoSpawner'

## Setup OIDC Auth.
auth_url = os.getenv('AUTH_URL', default = None)

if auth_url:
    if not auth_url.endswith('/'):
        auth_url = auth_url + '/'

    from oauthenticator.generic import GenericOAuthenticator
    c.JupyterHub.authenticator_class = GenericOAuthenticator

    import requests
    import json

    well_known_request = requests.get(auth_url + '.well-known/openid-configuration')
    well_known_request.raise_for_status()
    oidc_config = json.loads(well_known_request.content)

    c.GenericOAuthenticator.oauth_callback_url = os.getenv('OAUTH_CALLBACK_URI')
    c.GenericOAuthenticator.authorize_url = oidc_config['authorization_endpoint']
    c.GenericOAuthenticator.token_url = oidc_config['token_endpoint']
    c.GenericOAuthenticator.userdata_url = oidc_config['userinfo_endpoint']
    c.GenericOAuthenticator.username_key = os.getenv('OAUTH2_USERNAME_KEY', 'preferred_username')
    c.GenericOAuthenticator.client_id = os.getenv('CLIENT_ID', 'jupyterhub')
    secret = os.getenv('CLIENT_SECRET',default = None)
    if secret:
        c.GenericOAuthenticator.client_secret = secret
    else:
        logger.warning("No OIDC secret set.")
    c.GenericOAuthenticator.admin_users = {'admin'}
    c.GenericOAuthenticator.auto_login = True
else:
    raise Exception('No AUTH_URL environmental variable set. Cannot create secured server.')

# create system users that don't exist yet
import pwd
import subprocess
def pre_spawn_hook(spawner):
    username = spawner.user.name
    try:
        pwd.getpwnam(username)
    except KeyError:
        subprocess.check_call(['sudo useradd -ms /bin/bash "' + username + '"'], shell=True)

c.Spawner.pre_spawn_hook = pre_spawn_hook
c.Spawner.default_url = '/lab'
c.Spawner.environment = {
        'PATHDS_URL': 'localhost:5001',
        'COHORT_DB_STRING': 'mysql.foo',
        'LD_LIBRARY_PATH': '/usr/lib/jvm/java-1.8.0-openjdk-amd64/jre/lib/amd64/server'
}