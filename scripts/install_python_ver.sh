#!/bin/bash

PYTHON_VER=$1
PYTHON_CMD=$2
python_subver=${PYTHON_VER%.*}
echo ${PYTHON_VER}
if [ ! -z "${PYTHON_VER}" ];
then
    CONFIGURE_ARGS="--enable-shared"
    if [ "${OPTIMIZE_PYTHON}" == "true" ]
        then CONFIGURE_ARGS="$CONFIGURE_ARGS --enable-optimizations"
    fi
    PYTHON=Python-${PYTHON_VER}

    curl "https://www.python.org/ftp/python/${PYTHON_VER}/${PYTHON}".tgz --output "${PYTHON}".tgz
    tar -xf "${PYTHON}".tgz
    cd "${PYTHON}" || exit
    autoconf
    ./configure "$CONFIGURE_ARGS"
    make -j "${MAKE_WORKERS}"
    make altinstall
    ldconfig

    umask 022

    if [ "${INSTALL_MYPATIENT_PYTHON}" == "true" ];
        then ${PYTHON_CMD} -m pip install /modules/mypatient-python ; 
    fi
    if [ "${INSTALL_MRCNN}" == "true" ]; 
        then ${PYTHON_CMD} -m pip install /modules/mask_rcnn ; 
    fi

    ${PYTHON_CMD} -m pip install ipykernel
    ${PYTHON_CMD} -m ipykernel install --name ${PYTHON_CMD} --display-name "Python ${python_subver}"
    
fi