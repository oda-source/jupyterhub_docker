#!/bin/bash
update-ca-certificates -v

if [ ! -z "$WAIT_FOR" ]
then
    echo "Waiting for ${WAIT_FOR}..."


    IFS=':' read -r -a tokens <<< "$WAIT_FOR"
    

    while ! nc -z ${tokens[0]} ${tokens[1]}; do   
        sleep 0.5 # wait for 1/10 of the second before check again
    done

    echo "Port available"

fi
jupyterhub -f ${CONFIG_PATH}